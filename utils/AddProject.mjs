import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';
import { writeFile, mkdir } from 'node:fs/promises';
import { dump } from 'js-yaml'
import slugify from 'slugify'

const getMeta = async () => {
const rl = readline.createInterface({ input, output });
const description = await rl.question('Catchy, loud & visual description: ')
const title = await rl.question('Title: ')
const slug = slugify(title)
const tags = await rl.question('Tags: ').then(answer=> answer.split(',').map(entry=>entry.trim()))
const duration = await rl.question('Duration: ').then(answer=> answer.split(',').map(entry=>entry.trim()))
const git = await rl.question('Git: ')
// let links = [] // [...{title: string, url: string}]
const cover = await rl.question('Cover: ') // Maybe the cover could be a path / url to copy or download the file?
const coverAlt = await rl.question('Cover Description: ')
rl.close()

return {
    title, 
    description, 
    slug,
    tags, 
    duration, 
    git, 
    cover: {
        src: cover, 
        alt: coverAlt
        }
    }
}

const writeProject = async (project) => {
    try {
        // Creating the folder
        const projectFolder = new URL(`./content/projects/${project.slug}`, `file:${process.cwd()}/`);
        const createDir = await mkdir(projectFolder, { recursive: true });
        console.log(`Created ${createDir}`)

        // Writing the file
        let frontmatter = dump(project)
        let content = `---\n${frontmatter}---` // Ahah look at this
        writeFile(`${createDir}/${project.slug}.md`, content)
        console.log(`Success! Created ${project.slug}.md`)
    } catch (err) {
        console.log(err.message)
    }
}

const project = await getMeta()
await writeProject(project)

