## 22/07/2023

- Init Nuxt 3 app
- Link project to the [km0 repo](https://gitlab.com/km0/www)
- The script `yarn run new` executes `utils/AddProject.mjs`, a guided wizard to add new projects.
- Add the brainstorming for the website as first documentet project `/projects/km0/`

## 23/07/2023

- Add `changelog.md` to `/projects/km0`
- Homepage setup
