---
title: I need a website
description: A list of things I've done
slug: km0
tags:
  - web
  - research
  - documentation
duration:
  - 07/07/2023
  - ''
  - 19/07/2023
  - 20/07/2023
  - 21/07/2023
  - 22/07/2023
git: https://gitlab.com/km0
cover:
  src: ''
  alt: ''
---

It would be nice to have a website. To keep track and share with others what I'm busy with. 

It would be nice to create space to share processes, not just end results. 

It would be nice to preserve the different facets of things I do, and not to uniform everything as in a brand.

It would be nice to work on it one step at the time, to cultivate contents and letting them grow slowly. 

## Some ideas

Ok brainstorming here some ideas to see what could make sense given the poetic whishlist & constraints. 

### A list of things. 

One nice aspect of what I do is that there are so many different things, and when you think about it it's a bit crazy. Even just a short description for each could be interesting. For example: see the list in the background of [my final presentation at xpub](https://hub.xpub.nl/soupboat/~kamo/pres/), but slightly more curated and spectacular and catchy. 

Every description is a link to a project page with some documentation and info. 

**pro:** 
- works well for present and past projects
- it's a list
- preserve multiplicity of things

**mh:**
- more about results than process

### Annotated tags

A log where I can enter what I'm working on, with some tags. Like the [workinon](https://hub.xpub.nl/soupboat/workinon/). Things can be aggregated by tags. Tags can be annotated. This can be a really flexible and expressive system: a tag could be used to group different entries referring to the same project (ie: tag Hello Worlding collates together all the entries about the master thesis), but could be also used in a more transversal way (ie: all the works concerning performance).

**pro:**
- process oriented
- aggregate different tones
- unexpected pathways

**mh:**
- slow start
- difficult backtracking

## Constraints

There are some constraints:

- for a lot of past projects there is no good documentation
- lot of backtracking aka returning to old project

## Hosting others

I was wondering why it's so difficult for me to make a personal website. I mean: i make websites every day for everyone, it's really something that at the moment comes really natural to me. But why can't I take care of a space for my researches and projects? 

One possible reason is: because for me every project is a way to interact with others, to think together about something, to share ways to look at the world. And a personal website it's just, well, personal. Individual. Lonely. 

So maybe a different approach to this could be: a personal website where to host others. A space where to cultivate and archive collaborations. 

Sounds like an amazing concept but also a full time job ah ah.

## Let's start with the easy one

A list of things. Not a problem if of different nature or genre or whatever.

Ok ideally this could be the plan: 
- nuxt to fast prototype and kickstart the website.
- once the setup is there research on how to make it lighter and less resource intense.

Homepage is just a list of crazy things.

- Precise yet loud 1 liner descriptions.
- Some tools to filter and organize this list
    - tag
    - research
    - sort

What's the role of images in all of this? What about thumbnails?

## About dates & time

While doing some documentation workout during the XPUB master, I realized how difficult is to situate a project in time. 

Even projects with a precise restitutions such as performances or exhibitions can have a loong gestation, and it would be nice to render how much time and dedication these things involve. 

Here some notes from the [documentation workout](https://hub.xpub.nl/soupboat/~kamo/projects/soup2/):

> It doesn't always make sense to use just a date for framing a project. Especially for long-term prototypes or ongoing researches would be nice to have some more durational way of archiving.

These reflection on _durational archiving_ brought to these couple of ideas: 

![Two concept for dating projects: timelines and gradients. For the gradient one there are some images of this chemistry technique of chromatography, that reminds me a bit of github heatmap diagram. Follows a transcription of the text in the picture.](https://hub.xpub.nl/soupboat/~kamo/projects/soup2/tempo.jpg)


```
timeline

pros:
- easy to do
- just need start - end date

cons:
- it’s not really dynamic
- it’s an oversimplification of the temporailty of work


gradient

pros:
-   interesting
-   could be really expressive
-   could be developed as an habit:
    at the end of each week or day record on which project you worked on.
    think about it as a slightly more informative git heatmap - chromatography

cons:
-   uhm
-   requires a bit of engineering ----- (not strictly a cons though)
-   difficult to find the balance between something practical and something overcomplicated
```

### Gradient

About the gradient an approach could be to have a list of moments. 

Keep in mind that time working on a project and time updating the website are not the same thing.

Ok so this is a list of moments:

```
- 20/07/2023
- 19/07/2023
- 02/07/2023
- 14/05/2023
- 13/05/2023
- 11/02/2023
- 23/12/2022
- 21/12/2022
```

This list is not really homogeneous (wait does this word exist in english?): there is some difference between a couple of days in a row such as 19 and 20 of July, and then the 2nd of the month, let alone with the other updates two months before, or on february or even the previous year!

At the same time it's also true that the objective of this archive is not 1:1 surveillance: I don't care what did I do on the 13th of March 2016, but more interested in the fact that around those months I was writing a lot of songs. 

An useful trick for this durational notation could be the usage of a sparse array, with empty entries.

```
- 20/07/2023
- 19/07/2023
-
- 02/07/2023
-
-
- 14/05/2023
- 13/05/2023
-
-
- 11/02/2023
-
-
- 23/12/2022
- 21/12/2022
```

It's a total different restitution respect the previous one! The pauses between different clusters are totally arbitrary, and in a good way. A system like this permit to describe neat segments and blurred transitions alike.

Visually? Visually i don't know yet sorry.
