---
title: Spaghetti
slug: spaghetti
description: Research and development for cable-based web interactions
tags: Web, Interaction, Prototype
git: https://git.xpub.nl/kamo/spaghetti
duration:
    - 20/07/2023
    - 19/07/2023
    -
    - 02/07/2023
    -
    -
    - 14/05/2023
    - 05/2023
    -
    -
    - 11/02/2023
    -
    -
    - 07/2022
    - 06/2022
---

First attempt of using a cable-based interaction for web dasein. Research for the [Workbook](https://hub.xpub.nl/soupboat/workbook/).

See also the implementation in [panel](https://git.xpub.nl/kamo/panel)!

